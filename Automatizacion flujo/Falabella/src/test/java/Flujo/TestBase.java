package Flujo;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public abstract class TestBase {
	protected WebDriver driver;
	
	
	@Before
	public void inicio() {
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--start-maximized");
		//Ingresar como incognito//
		//options.addArguments("--incognito");//
		System.setProperty("webdriver.chrome.driver", "C:\\chromedriver.exe");
		this.driver = new ChromeDriver(options);
	}

	@After
	public void fin() {
		Utils.sleep(4);
		this.driver.close();
	}

}
