package Flujo;



import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import Flujo.TestBase;
import Flujo.Utils;


	
public class Falabella extends TestBase {
	
	private final String url = "https://www.falabella.com/falabella-cl/";
	private int contador = 1;

	@Test
	public void Ingreso() {
		final JavascriptExecutor js = (JavascriptExecutor) driver;
		driver.get(url);
		Utils.sleep(3);
		
		try {
			final WebElement cierrePOPup = this.driver.findElement(By.xpath("/html/body/div[5]/div[2]/div/div[1]"));
			cierrePOPup.click();
			Utils.sleep(1);
			Utils.pantallazo(driver, "Evidencia" + contador ++);
		} catch (Exception e) {
			// TODO: handle exception
		}
	
		final WebElement categorias = this.driver.findElement(By.xpath("//*[@id=\"hamburgerMenu\"]/div[1]"));
		categorias.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement tecnologia = this.driver.findElement(By.xpath("//*[@id=\"item-3\"]/div[1]"));
		tecnologia.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement consola = this.driver.findElement(By.xpath("//*[@id=\"header\"]/nav/div[1]/div[1]/div[1]/div/section[1]/div/section[4]/div/div[1]/ul[5]/div[2]/li[3]/a"));
		consola.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		js.executeScript("scroll(0, 650);");
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement marca = this.driver.findElement(By.xpath("//*[@id=\"testId-Accordion-Marca\"]"));
		marca.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement checkboxMarca = this.driver.findElement(By.xpath("//*[@id=\"BANDAI-5\"]"));
		checkboxMarca.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement verProducto = this.driver.findElement(By.xpath("//*[@id=\"testId-Pod-action-12593505\"]"));
		verProducto.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement agregarBolsa = this.driver.findElement(By.xpath("//*[@id=\"buttonForCustomers\"]/button"));
		agregarBolsa.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
		final WebElement aunmento = this.driver.findElement(By.xpath("//*[@id=\"__next\"]/div/div/div/div/div/div/div[2]/div/div/div/div/div[3]/div/button[2]"));
		aunmento.click();
		aunmento.click();
		Utils.sleep(1);
		Utils.pantallazo(driver, "Evidencia" + contador ++);
		
				
		System.out.println("Test OK");
	
	}

	public void fin() {
		
		driver.close();
		//fail("Not yet implemented");	
}
}


